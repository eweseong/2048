/**
 * Two O Four Eight Javascript API
 * @version 1.0
 * @namespace TOFE
 * @author Yeoh Ewe Seong
 */
(function(window, $) {
	"use strict";
	
	if (window.TOFE) {
		return;
	}
	
	var init = false,
		tofeOpt = {},
		tofe = window.TOFE = function() {},
		$target = {},
		$board = {},
		data_score = 'score',
		data_won = 'won';
	
	tofe.init = function(options) {
		// prevent double initialization
		if (init) {
			return false;
		}
		
		console.log('Initializing game...');
		
		init = true;
		
		tofeOpt = $.extend(true, {
			target: '',
			gridSize: 4,
			startingTile: 2,
			winValue: 2048
		}, options);
		
		if (tofeOpt.startingTile > tofeOpt.gridSize * tofeOpt.gridSize) {
			init = false;
			alert('initialization failed');
		}
		
		// setup game data
		$target = $(tofeOpt.target).data(data_score, 0);
		$board = new Board(tofeOpt.gridSize);
		
		// setup game layout
		drawDialog();
		drawTitle();
		drawBoard();
		
		console.log('Initialization complete');
		
		return tofe;
	};
	
	tofe.startGame = function() {
		console.log('Starting game...');
		
		bindNewGameEvent();
		bindKeyboardEvent();
		bindSwipeEvent();
		
		addStartingTiles();
		
		console.log('Game Started');
		return tofe;
	};
	
	tofe.resetGame = function() {
		$board = new Board(tofeOpt.gridSize);
		
		drawTitle();
		drawBoard();
		
		$target.data(data_score, 0).data(data_won, false).data('gg', false);
		
		paintScoreBoard();
		addStartingTiles();
	};
	
	tofe.alertDialog = function(text, callback) {
		var $dialogContains = $('.game-backdrop, .game-dialog-wrapper');
		
		$('#gameDialog').html(text);
		$dialogContains.addClass('in');
		
		$('.game-dialog-wrapper').one('click', function(e) {
			console.log(e);
			
			$dialogContains.removeClass('in');
			if ($.isFunction(callback)) {
				callback();
			}
		});
	}
	
	var drawDialog = function() {
		$('body').append('<div class="game-backdrop"></div><div class="game-dialog-wrapper"><div id="gameDialog"></div>Press anywhere to continue</div>');
	};
	
	var drawTitle = function() {
		$target.html('<div class="game-wrapper">' + 
						'<div class="game-header">' + 
							'<div class="clearfix">' + 
								'<h1 class="game-title">2048</h1>' + 
								'<div class="game-score-board">Score <div id="gameScoreCard"></div></div>' + 
							'</div>' + 
							'<button type="button" id="btnNewGame">New Game</button>' +
						'</div>' + 
					'</div>');
		
		paintScoreBoard();
	};
	
	var drawBoard = function() {
		$target
		.find('.game-wrapper')
		.append('<div id="gameBoard">' +
					'<div>' + drawGrid() + '</div>' + 
					'<div id="gameTileWrapper"></div>' + 
				'</div>' + 
				'<label>Adapted from</label> <a href="http://gabrielecirulli.github.io/2048/">Gabriele Cirulli 2048</a>');
	};
	
	var drawGrid = function() {
		var grid = '',
			size = tofeOpt.gridSize;
		
		// creating grid rows
		for (var row = 0; row < size; ++row) {
			grid += '<div class="grid-row">';
			
			// creating grid columns
			for (var col = 0; col < size; ++col) {
				grid += '<div class="grid-col"></div>';
			}
			grid += '</div>';
		}
		
		return grid;
	};
	
	var addStartingTiles = function() {
		for (var i = 0; i < tofeOpt.startingTile; ++i) {
			addRandomTile();
		}
	};
	
	var addRandomTile = function() {
		if ($board.hasEmptySquare()) {
			// new tile value is assign randomly between 2 and 4, with a rough percentage of 90% : 10% chance
			var tile = new Tile($board.getRandomEmptySquare(), Math.random() < .9 ? 2 : 4);
			
			$board.addTile(tile);
			drawNewTile(tile);
		}
	};
	
	var drawNewTile = function(tile) {
		$target.find('#gameTileWrapper').append('<div class="tile tile-position-' + tile.r + '-' + tile.c + ' tile-' + tile.value + ' new-tile"><div class="tile-content">' + tile.value + '</div></div>');
	};
	
	var bindNewGameEvent = function() {
		$target.on('click', '#btnNewGame', function(e) {
			tofe.resetGame();
		});
	};
	
	var UP = 0, RIGHT = 1, DOWN = 2, LEFT = 3;
	
	// Returns a function, that, as long as it continues to be invoked, will not
	// be triggered. The function will be called after it stops being called for
	// N milliseconds. If `immediate` is passed, trigger the function on the
	// leading edge, instead of the trailing.
	function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};
	
	var bindKeyboardEvent = function() {
		$(document).keydown(debounce(function(e) {
			switch(e.which) {
				case 38: // up
				case 87: // W
				move(UP);
				break;

				case 39: // right
				case 68: // D
				move(RIGHT);
				break;

				case 40: // down
				case 83: // S
				move(DOWN);
				break;
				
				case 37: // left
				case 65: // A
				move(LEFT);
				break;

				default: return; // exit this handler for other keys
			}
			
			e.preventDefault(); // prevent the default action (scroll / move caret)
		}, 50));
	};
	
	var bindSwipeEvent = function() {
		var hammer = new Hammer(document.getElementById('gameBoard'));
		
		hammer.get('swipe').set({ direction: Hammer.DIRECTION_ALL });
		hammer.on('swipeup swiperight swipedown swipeleft', function(e) {
			if ('swipeup' === e.type) {
				move(UP);
			}
			else if ('swiperight' === e.type) {
				move(RIGHT);
			}
			else if ('swipedown' === e.type) {
				move(DOWN);
			}
			else if ('swipeleft' === e.type) {
				move(LEFT);
			}
		});
	};
	
	var move = function (direction) {
		var vector = getDirectionMovement(direction),
			rStart = vector.rStart, rIncrement = vector.rIncrement,
			cStart = vector.cStart, cIncrement = vector.cIncrement,
			squares = $board.squares,
			tile, nextTile, mergedTile,
			furthestPosition,
			isMoved = false;
			
		for (var r = rStart; isWithinBound(r); r = r + rIncrement) {
			for (var c = cStart; isWithinBound(c); c = c + cIncrement) {
				tile = squares[r][c];
				
				// find furthest available cell
				if (tile) {
					// reset indicators
					mergedTile = false;
					furthestPosition = false;
					
					// TODO should simplify the find furthest logic
					if (direction === UP) {
						for (var r2 = r - 1; isWithinBound(r2); r2 = r2 - rIncrement) {
							nextTile = squares[r2][c];
							
							if (nextTile) {
								if (mergedTile || nextTile.mergedWith || tile.value !== nextTile.value) {
									break;
								}
								
								mergedTile = nextTile
							}
							
							furthestPosition = {r: r2, c: c};
						}
					}
					else if (direction === RIGHT) {
						for (var c2 = c + 1; isWithinBound(c2); c2 = c2 + rIncrement) {
							nextTile = squares[r][c2];
							
							if (nextTile) {
								if (mergedTile || nextTile.mergedWith || tile.value !== nextTile.value) {
									break;
								}
								
								mergedTile = nextTile
							}
							
							furthestPosition = {r: r, c: c2};
						}
					}
					else if (direction === DOWN) {
						for (var r2 = r + 1; isWithinBound(r2); r2 = r2 - rIncrement) {
							nextTile = squares[r2][c];
							
							if (nextTile) {
								if (mergedTile || nextTile.mergedWith || tile.value !== nextTile.value) {
									break;
								}
								
								mergedTile = nextTile
							}
							
							furthestPosition = {r: r2, c: c};
						}
					}
					else if (direction === LEFT) {
						for (var c2 = c - 1; isWithinBound(c2); c2 = c2 - rIncrement) {
							nextTile = squares[r][c2];
							
							if (nextTile) {
								if (mergedTile || nextTile.mergedWith || tile.value !== nextTile.value) {
									break;
								}
								
								mergedTile = nextTile
							}
							
							furthestPosition = {r: r, c: c2};
						}
					}
					
					if (furthestPosition) {
						isMoved = true;
						$board.removeTile(tile);
						
						if (mergedTile) {
							$board.removeTile(mergedTile);
							if (mergedTile.previousPosition) {
								mergedTile.updatePosition(mergedTile.previousPosition);
							}
							tile.merge(mergedTile);
						}
						
						tile.updatePosition(furthestPosition);
						$board.addTile(tile);
					}
				}
			}
		}
		
		// for debug purpose
		$target.data('moved', isMoved);
		
		if (isMoved) {
			paintBoard(direction);
		
			$board.resetTiles();
			
			// add tiles after moving animation ends
			setTimeout(function() {
				addRandomTile();
				
				if (!haveMovesLeft()) {
					$target.data('gg', true);
					tofe.alertDialog('End Game', tofe.resetGame);
				}
			}, 110);
		}
	};
	
	var getDirectionMovement = (function() {
		var directionVector = {};
		
		directionVector[UP] = {
			rStart: 1,
			rIncrement: 1,
			cStart: 0,
			cIncrement: 1
		};
		
		directionVector[RIGHT] = {
			rStart: 0,
			rIncrement: 1,
			cStart: 2,
			cIncrement: -1
		};
		
		directionVector[DOWN] = {
			rStart: 2,
			rIncrement: -1,
			cStart: 0,
			cIncrement: 1
		};
		
		directionVector[LEFT] = {
			rStart: 0,
			rIncrement: 1,
			cStart: 1,
			cIncrement: 1
		};
			
		return function(direction) {
			return directionVector[direction];
		};
	})();
	
	var isWithinBound = function(index) {
		return index < tofeOpt.gridSize && index > -1;
	};
	
	var paintBoard = function(direction) {
		var start = 0,
			increment = 1,
			tiles = $board.getTiles(),
			scoreAdded = 0,
			wonGame = false;
			
		if (direction === DOWN || direction === RIGHT) {
			start = tiles.length - 1;
			increment = -1;
		}
		
		for (var i = start; i >= 0 && i < tiles.length; i = i + increment) {
			var tile = tiles[i];
			
			if (tile.mergedWith) {
				var $tile = $target.find('.tile-position-' + tile.mergedWith.r + '-' + tile.mergedWith.c).remove();
				scoreAdded += tile.value;
				
				if (tile.value >= tofeOpt.winValue) {
					wonGame = true;
				}
			}
			
			if (tile.previousPosition) {
				$tile = $target.find('.tile-position-' + tile.previousPosition.r + '-' + tile.previousPosition.c);
				$tile.removeClass().addClass('tile tile-' + tile.value + ' tile-position-' + tile.r + '-' + tile.c).html('<div class="tile-content">' + tile.value + '</div>');
			}
		}
		
		if (scoreAdded) {
			updateScore(scoreAdded);
			paintScoreBoard();
		}
		
		if (wonGame && !$target.data(data_won)) {
			$target.data(data_won, true);
			tofe.alertDialog('You Win');
		}
	};
	
	var paintScoreBoard = function() {
		$target.find('#gameScoreCard').html($target.data(data_score));
	};
	
	var updateScore = function(scoreAdded) {
		$target.data(data_score, $target.data(data_score) + scoreAdded);
	};
	
	var haveMovesLeft = function() {
		return $board.hasEmptySquare() || hasMatchingTile();
	};
	
	var hasMatchingTile = function() {
		var squares = $board.squares;
		
		for (var r = 0; r < tofeOpt.gridSize; ++r) {
			for (var c = 0; c < tofeOpt.gridSize; ++c) {
				var tile = squares[r][c];
				
				if (tile) {
					for (var direction = 0; direction < 4; ++direction) {
						var vector = getVector(direction);
						
						if (isWithinBound(r + vector.r) && isWithinBound(c + vector.c)) {
							var neighbourSquare = squares[r + vector.r][c + vector.c];
							
							if (neighbourSquare && tile.value == neighbourSquare.value) {
								return true;
							}
						}
					}
				}
			}
		}
		
		return false;
	};
	
	var getVector = (function() {
		var map = {
			0: { r: 0,  c: -1 }, // Up
			1: { r: 1,  c: 0 },  // Right
			2: { r: 0,  c: 1 },  // Down
			3: { r: -1, c: 0 }   // Left
		};
			
		return function(direction) {
			return map[direction];
		};
	})();
	
	window['TOFE'] = tofe;
})(window, window.jQuery);