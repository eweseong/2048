/**
 * @reference - http://gabrielecirulli.github.io/2048/js/grid.js
 */

function Board(size) {
	this.size = size;
	this.squares = this.reset();
};

Board.prototype.reset = function () {
	var squares = [];
		
	for (var r = 0; r < this.size; ++r) {
		squares[r] = [];
		
		for (var c = 0; c < this.size; ++c) {
			squares[r].push(null);
		}
	}
	
	return squares;
};

Board.prototype.hasEmptySquare = function () {
	return this.getEmptySquares().length;
};

Board.prototype.getRandomEmptySquare = function () {
	var emptySquares = this.getEmptySquares();
	
	if (emptySquares.length) {
		return emptySquares[Math.floor(Math.random() * emptySquares.length)];
	}
};

Board.prototype.getEmptySquares = function () {
	var emptySquares = [];
	
	this.forEach(function(r, c, tile) {
		if (!tile) {
			emptySquares.push({r: r, c: c});
		}
	});
	
	return emptySquares;
};

Board.prototype.addTile = function (tile) {
	this.squares[tile.r][tile.c] = tile;
};

Board.prototype.removeTile = function (tile) {
	this.squares[tile.r][tile.c] = null;
};

Board.prototype.getTiles = function () {
	var tiles = [];
	
	this.forEach(function(r, c, tile) {
		if (tile) {
			tiles.push(tile);
		}
	});
	
	return tiles;
};

Board.prototype.resetTiles = function () {
	this.forEach(function(r, c, tile) {
		if (tile) {
			tile.mergedWith = null;
			tile.previousPosition = null;
		}
	});
};

/** 
 * @function - util function to lool through all squares
 */
Board.prototype.forEach = function(callback) {
	for (var r = 0; r < this.size; ++r) {
		for (var c = 0; c < this.size; ++c) {
			callback(r, c, this.squares[r][c]);
		}
	}
};

Board.prototype.toString = function() {
	var s = '',
		counter = 0;
	
	this.forEach(function(r, c, tile) {
		if (counter !== r) {
			counter = r;
			s += '\n';
		}
		
		s += '[' + (tile ? tile.value : ' ') + ']'
	});
	
	return s;
};
