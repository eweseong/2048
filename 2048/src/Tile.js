/**
 * @reference - http://gabrielecirulli.github.io/2048/js/tile.js
 */
 
function Tile(position, value) {
	this.updatePosition(position);
	
	this.value = value || 2;
	this.mergedWith = null;
	this.previousPosition = null;
};

Tile.prototype.updatePosition = function(position) {
	this.previousPosition = {r: this.r, c: this.c};
	
	this.r = position.r;
	this.c = position.c;
	
	return this;
};

Tile.prototype.merge = function(tile) {
	this.value *= 2;
	this.mergedWith = tile;
	
	return this;
};

Tile.prototype.serialize = function() {
	return {
		position: {
			r: this.r,
			c: this.c
		},
		value: this.value,
		mergedWith: this.mergedWith,
		previousPosition: this.previousPosition
	}
};
