# Info
> This game is adapted from http://gabrielecirulli.github.io/2048/.
> I claim no rights to the concept of the game.

# Compatibility
 Google Chrome | Google Chrome(Android) | Mozilla Firefox | Internet Explorer
:-------------:|:----------------------:|:---------------:|:-----------------:
37+			   | 			37+		    | 30+			 | 10+